#![allow(clippy::empty_loop)]
#![no_main]
#![no_std]


extern crate panic_halt;
use cortex_m::{
    // I am using the delay function because it allows me to set a a delay of 
    // a certain number of clock cycles.
    // I use this in the loop cycle later on.
    asm::delay,
};
use cortex_m_rt::{
    // This provides the "entry-point"
    entry
};
use cortex_m_semihosting::{
    //hprintln allows me to get messages back via the StLinkV2 dongle
    hprintln
};
use stm32f4::stm32f411::{
    Peripherals
};
//use stm32f4xx_hal::{
//    pac,
//    prelude
//};





#[entry]
fn main() -> ! {

    //Just a quick message via the debug semihosting system
    //Lets me know that the microcontroller has started up
    hprintln!("Device Starting:");

    //
    // PERIPHERALS
    //

    // When we are coding the microcontroller, we are telling it what to do
    // with the various 'peripherals' or parts that is has available.
    // The first thing we need to do is get access to the peripherals.
    let device_peripherals = Peripherals::take().unwrap();

    // Of all of the peripherals of the microcontroller, we need use:
    // RCC = Reset and Clock Control (Reference Manual, Chapter 6, page 90)
    // Flash = Embedded Flash Memory (Ref. Man., Ch.3, p.43)
    let flash = device_peripherals.FLASH;
    let rcc = device_peripherals.RCC;
    // RCC and FLASH are two peripherals on the STM32F411 (and probably all
    // STM32 microcontrollers)

    //
    // REGISTERS
    //

    // To control the peripherals on the microcontroller, we interact with
    // 'registers' which are special locations in the microcontroller's memory.

    // Since this example is all about setting up the microcontroller to run
    // at 100MHz, it makes sense that we need to set up the system clock.
    // Recall that we are going to interact with the "Reset and Clock Control"
    // register.
    // Figure 13 (page 93) shows how the clock of the microcontroller is
    // organised.

    // The left hand side of the figure shows the inputs and outputs of the
    // microcontroller - these inputs and outputs will map to pins of the 
    // physical microcontroller.
    // For example, OSC_IN and OSC_OUT on Figure 13 of the Reference Manual
    // are physical pins 5 and 6 on the STM32F411CE used on the 'BlackPill'.
    // We have a 25MHz crystal connected to these pins.

    // The right hand side of Figure 13 shows which other parts of the 
    // microcontroller are given a clock signal.
    // We are going to be interested in "HCLK" - the CPU clock.
    // This example is all about getting that to 100MHz.

    // The stuff in the middle of Figure 13 is shows the parts of the 
    // Reset and Clock Control register that we will need to set in order
    // to get from a 25MHz external crystal on pins 5 and 6 to a 100MHz
    // CPU clock frequency.
    // I will attempt to describe the 'flow' of the clock signal:
    // 1) From the OSC_OUT and OSC_IN pins and the box which reads "4-26MHz 
    // HSE OSC", follow the HSE line horizontally to the right, then down,
    // then to the left to a trapezoid.
    // 2) This trapezoid means 'switch' (so we could select between the HSE and
    // the lower speed HSI)
    // 3) After the switch, we go through the box marked "/M" which means that
    // the signal is divided 
    // 4) After "/M", the signal goes into the PLL "Phase Locked Loop" which 
    // changes te frequency of the incoming signal by:
    //      4a) multiplying the signal by "xN"
    //      4b) dividing it again by "/P"
    // 5) After the signal is processed by the PLL (divided by M, multiplied 
    // by N, and divided by P), we follow the line running horizontally to the
    // right and then up to another switch named "SW" with the input labelled
    // "PLLCLK"
    // 6) Next to the "SW" switch we can see a note saying that the system
    // needs to be 100MHz max - so the output of the PLL cannot exceed 100MHz.
    // 7) The signal goes through something called "AHB prescaler"
    // 8) We follow the signal out to the right and up to a semi-circle which
    // probably means 'output'. It needs to be a maximum of 100MHz and we can
    // see that the output needs to be enabled because there is a another 
    // line running to the semi-circle labelled "Clock Enable"
    //
    // So from following this chart, we need to enable or set:

    // Turn on:
    //      - HSE
    //      - PLL

    // Select Inputs:
    //      - HSE as PLL input
    //      - PLL as SYSCLK input
    
    // Set division/ multiplication ratios:
    //      - "/M" - PLL input prescaler
    //      - "xN" - PLL VCO multiplier
    //      - "/P" - PLL output divider
    //      - AHB Prescaler

    //      For the sake of brevity, I am skipping over the fact that:
    //      a) the microcontroller has a basic 16MHz internal oscillator
    //      (Sec. 6.2.2),
    //      b) the fact that we can provide the microcontroller a clock
    //      signal from the internal clock, an external clock, or either of
    //      these clocks via the PLL (phase lock loop) (Sec. 6.2), and
    //      c) how the internal (Sec 6.2.2) and external clocks (Sec 6.2.1)
    //      operate.


    // 
    // MODIFYING BITS
    //

    // So now that we know what we need to do (th elist above), let's look at
    // how to do it.
    // Here we interact with the microcontroller by changing bits within
    // registers.

    // Next to OSC_IN and OSC_OUT, we have a little square that reads:
    // "4-26MHz HSE OSC" - code for "4MHz to 26MHz High Speed External
    // Oscillator".
    // Reading Sec 6.2.1, go to the bottom of page 95.
    // There we can see how we turn on the "High Speed External" clock
    // and how the microcontroller knows that the HSE is active.
    // We turn the HSE on (or off) by changing the "HSEON" bit in the
    // "RCC_CR" (Clock control) register, and we know whether it is ready
    // by reading the HSERDY bit in the same register.
    // A register is a collection of bits, some of which change to turn things
    // on and off (such as the HSEON bit), and others we read (e.g. HSERDY bit)

    //
    // FINDING THE BITS WE NEED TO FLIP
    //

    // IN THE DATASHEET
    // https://www.st.com/resource/en/reference_manual/rm0383-stm32f411xce-advanced-armbased-32bit-mcus-stmicroelectronics.pdf
    // - RCC is the peripheral (Section 6.3, page 102).
    // - RCC_CR is a 32-bit register within the RCC peripheral (Section 6.3.1, 
    // page 102).
    // - HSEON is bit number 16 in the RCC_CR register.

    // IN THE RUST STM32F4 PERIPHERAL ACCESS CRATE
    // https://docs.rs/stm32f4/0.14.0/stm32f4/
    // (NOTE: not all microcontrollers are listed on the documentation page)
    // Recall that earlier we imported stm32f4::stm32f411::Peripherals.
    // Then we accessed the peripherals.
    // And then created a variable (rcc) which accessed device_peripherals.RCC.
    // Looking at the documentation for the rcc module at:
    // https://docs.rs/stm32f4/0.14.0/stm32f4/stm32f407/rcc/index.html
    // (Not the STM32F411, but the F407 - similar enough)
    // we can see that the "clock control register" is called "cr".

    // If you clicked on the link for "rc" - I don't know how you would find
    // out the way to modify the register.
    // But if you go to the general register page:
    // https://docs.rs/stm32f4/0.14.0/stm32f4/struct.Reg.html
    // There are a few functions:
    //      - as_ptr - reports the location in memory of the register 
    //      - read - does what it says on the packet
    //      - reset - resets the bits to their default state
    //      - write - you would think that this is the one we want...
    //      - write_with_zero - I'm not sure what this is meant to do.
    //      - modify - ...but this is the one we want!
    // 
    // It turns out that you can't just simply change one or two bits in a
    // register on the STM32. 
    // You need to write either the whole register or perhaps just the byte.
    // (I don't fully understand how much of the register you need to change,
    // all I know is that you can't change just one bit).

    // The write function is used to write to the whole register (all 32 bits).
    // You could use this to turn on a whole array of ports.
    // It can also write a few bits, and set the rest to zero.
    // The downside of this is that the write function will overwrite anything
    // that was originally set, unless you re-write it to the register.
    // E.g.:
    //      - If the original is:   0b00110000
    //      - And you write:        0b10000000 
    //               - or (1 << 7) or (0x80)
    //      - You will end up with: 0b10000000
    //               - overwriting all other bits

    // So we use the "modify" function.
    // This:
    // 1) "reads" the register
    // 2) "modifies" the register according to your wishes
    // 3) "writes" back the modified register
    // So:
    //      - If the original is:                  0b00110000
    //      - And you want to toggle bit #7:       0b10000000
    //      - To end up with this:                 0b10110000
    // 1) The microcontroller reads the original value,
    // 2) Does the bitwise arithmetic on the original value and the new value,
    // 3) Writes back the modified value
    
    hprintln!("Turnign on HSE clock source");

    rcc.cr.modify(|_, w| w.hseon().on());
    // rcc      - peripheral
    // cr       - register
    // modify() - function
    // |_, w|   - I don't know what this is - I assume it is a way to tell
    //              the microcontroller to read and write the same bits...
    // w.       - telling the modify function that we want to write 
    // hseon()  - the bits we want to modify
    // on()     - what we want to bits to become

    // If we go to the cr page:
    // https://docs.rs/stm32f4/0.14.0/stm32f4/stm32f407/rcc/cr/index.html
    // We can see a rust struct called "HSEON_W". Going to that page
    // https://docs.rs/stm32f4/0.14.0/stm32f4/stm32f401/rcc/cr/struct.W.html
    // Shows us that hseon() has several functions:
    //      - variant - I don't know what this is
    //      - off = "0"
    //      - on = "1"
    //      - set_bit - same as "1"?
    //      - clear_bit - same as "0"?
    //      - bit - write your own bits to this field

    // So this was a bit of a journey.
    // After reading through a section of the data sheet to divine what we
    // would need to enable in order to get the clock set up and write a list
    // of what we think we need to do...
    // we look for a chapter in the datasheet which covers the peripheral which
    // contains the register which has the bits we need to toggle.
    // Then we go to the documentation of the peripheral access crate.
    //      STM32F4
    // We find a 'module' whose name matches the peripheral.
    //      rcc
    // Under that module, we find another module whose name matches the 
    // register.
    //      rcc.cr
    // We choose the function we want to apply to that register
    //      rcc.cr.modify(|_, w| w.)
    // In that module, we find a struct whose name is similar to the field of 
    // bits that we want to toggle
    //      HSEON_W
    // But in code, it will be just the name of the field
    //       rcc.cr.modify(|_, w| w.hseon())
    // The struct whose name is similar to the field of bits we want to toggle
    // gives us the available functions from which we choose
    //       rcc.cr.modify(|_, w| w.hseon().on())
    // And don't forget a semicolon
    //       rcc.cr.modify(|_, w| w.hseon().on());
    //
    // Unfortunately, we will have to cover this ground every time we want to
    // modify a register.
    // But thankfully, if you have set up some kind of IDE, you will get 
    // some options autocompleted to help you find the right peripherals,
    // registers, functions, fields, and options.

    // NOTE:
    // Unfortunately, I could not find anywhere cookbook that documents
    // which registers you need to interact with in order to get started.
    // The order in which I have documented this code is the result of
    // reading lots of examples (some better than others) and LOTS of trial 
    // and error. 
    // Mostly error.

    // This loop will be discussed later on in the code.
    // For now, all we need to know is that it is uses the 'read()' function
    // instead of the 'modify()' function we used earlier.
    // It is reading the 'hserdy()' bits to see if they match the value of 
    // the 'is_ready()' function.

    for i in 0..100 {
        hprintln!("Waiting for HSE Clock, round {}", i);
        if rcc.cr.read().hserdy().is_ready() {
            hprintln!("HSE Clock Ready");
            break;
        }
        if i > 95 {
            hprintln!("HSE Clock initiailisation failed")
        }
    }

    //
    // SETTING UP FLASH MEMORY
    //

    hprintln!("Setting up flash memory for >50MHz clock speed");

    flash.acr.modify(|_, w| w
        .dcen().enabled()
        .icen().enabled()
        .prften().enabled()
    );
    //We have to add wait states to accessing the flash memory if we want to run >30MHz
    //Refer to page 45, Section 3.4.1, Table 5
    flash.acr.modify(|_, w| w.latency().ws3());

    hprintln!("Setting up system clock dividers and switches");   

    rcc.cfgr.modify(|_, w| w
        .hpre().div1()
        .sw().pll()
    );
    // Clock options
    // Section 6.3.3, page 106

    hprintln!("Setting up PLL");

    rcc.pllcfgr.modify(|_, w| w.pllsrc().hse());    
    // Telling the PLL that we want to use the HSE as its source

    // We have to set up the PLL options BEFORE we turn it on.

    unsafe {
        //Set PLLM (Input division for main input clock) to 12
        //i.e. take 25MHz input and divide by 12
        //The PLL recommends an input freq of 2MHz
        rcc.pllcfgr.modify(|_, w| w.pllm().bits(0b001100)); // divide by 12
        //Set PLLN to *96
        rcc.pllcfgr.modify(|_, w| w.plln().bits(0b001100000)); // multiply by 96
        //rcc.pllcfgr.modify(|_, w| w.plln().bits(1 << 8));
        //Set PLLP to /2
        rcc.pllcfgr.modify(|_, w| w.pllp().bits(0b00)); // divide by 2
        //rcc.pllcfgr.modify(|_, w| w.pllp().bits(01));
        //This gives an out frequency of:
        // 25MHz / 12  = 2.08333 MHz * 96 = 200MHz / 2 = 100MHz
    }
    


    //we turn on the PLL AFTER we set it up

    rcc.cr.modify(|_, w| w.pllon().set_bit());

    //
    // Loops while waiting for the microcontroller
    //

    // Now we set up a loop to keep the microcontroller busy while we wait 
    // for the PLL to become ready.
    // In other words we want the microcontroller to 'do something while' 
    // waiting for a particular condition to become true.
    // In this case, the 'something' we get the microcontroller to 'do' is to 
    // print the line "Waiting for PLL"
    // The condition we want to check is whether the PLL is ready.
    // We check whether the PLL is ready by using the 'read()' function to 
    // read the 'pllrdy()' bits and check them using the 'is_ready()' function.
    
    // When I was originally developing this code I used a "while" loop:

    /*
    while !rcc.cr.read().pllrdy().is_ready() {}
    */
    
    // And it worked just fine.
    // While the condition "rcc.cr.read().pllrdy().is_ready()" was not true
    // (the "!" symbol), the microcontroller would run an empty loop.
    // But there's a catch in this loop.
    // It does nothing, literally nothing.
    // As a result, the compiler may decide to strip this line out because
    // there is no output in this loop.
    // We could get around that by putting a simple 'delay()' function or
    // perhaps print a line to output.
    // But we have a way of avoiding the possibility that the compiler
    // will try to make our code smaller by stripping out what could be 
    // an unnecessary loop.

    // The "loop"
    // The advantage of "loop" is that it will always run at least once.
    // It also appears (on the two sites linked below) that the generally 
    // accepted way to have a do-something-while loop in Rust is to use the
    // the "loop" function.

    // https://stackoverflow.com/questions/28892351/what-is-the-difference-between-loop-and-while-true

    // https://www.reddit.com/r/rust/comments/1v9rgp/rust_has_dowhile_loops/

    // This loop uses a 'break' in order to tell the loop to stop running.
    // If the PLL is ready, we 'break' the loop and let the microcontroller
    // execute the next lines of code outside the loop.
    // If the PLL is not ready, the loop will repeat.

    /*
    loop {
        hprintln!("Waiting for PLL");
        if rcc.cr.read().pllrdy().is_ready() {
            break;
        }
    }
    */

    // The drawback of this loop is that it could become infinite and run 
    // forever.
    // For example, if the "pllrdy()" bit never becomes "is_ready()", then
    // the loop will continue forever and the microcontroller will never
    // execute any further lines of code.
    // All you will see is that the microcontroller has... stopped...

    // Another possibility is to use a "for" loop which will repeat up to a 
    // certain number of times, unless it encounters a "break".
    // In this example, if the pllrdy bit becomes ready, we print a message
    // and then break the loop.
    // But if the PLL bit never becomes ready, then we can print a message 
    // saying that something has gone wrong and let the loop continue 
    // until we reach the maximum number of iterations.
    // This prevents us from having a situation where the loop continues
    // indefinetly because the 'condition' never becomes 'true'.
    
    for i in 0..100 {
        hprintln!("Waiting for PLL, round {}", i);
        if rcc.cr.read().pllrdy().is_ready() {
            hprintln!("PLL READY");
            break;
        }
        if i > 95 {
            hprintln!("PLL initialisation failed")
        }
    }

    hprintln!("Microcontroller continuing");
    


    loop {
        // Set the delay to be 10,000,000 clock cycles
        delay(10_000_000);
        // Then print "Hello again" via semihosting so that I know
        // that the microcontroller has started up and is running the loop
        // If you change the timing settings above, you will see that the
        // frequency at which the message is printed will change.
        // This is because the delay is always 10,000,000 clock cycles
        // On a 1MHz clock, this will be 10 seconds,
        // On a 10MHz clock, this will be 1 second,
        // and so on...
        hprintln!("Hello again");
    }

}
