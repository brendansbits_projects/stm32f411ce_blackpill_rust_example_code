# Example 2 - Adding 2Mbps UART

Building upon he first example where we set up a 100MHz system clock, now I want to enable UART output.

## UART

According to the datasheet (Section 3.2.2, pages 29-30), there are three USART ports, 1, 2, and 6.
1 and 6 run at a higher speed, 2 at a lower speed.

The code in the file is a minimum working example of getting serial output from USART 1 via pins A9 and A10.
It prints a stream of ASCII characters to your serial console.
Most importantly, it gets a baud rate of 2,000,000 which worked on linux with the CH431 Serial to USB chip.
