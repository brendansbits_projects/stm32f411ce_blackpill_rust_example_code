#![allow(clippy::empty_loop)]
#![no_main]
#![no_std]


extern crate panic_halt;

use cortex_m::{
    asm::delay,
};
use cortex_m_rt::{
    entry
};
use cortex_m_semihosting::{
    hprintln
};
use stm32f4::stm32f411::{
    Peripherals,
};


#[entry]
fn main() -> ! {

    hprintln!("Device Starting:");

    let device_peripherals = Peripherals::take().unwrap();

    // RCC = Reset and Clock Control (Reference Manual, Chapter 6, page 90)
    // Flash = Embedded Flash Memory (Ref. Man., Ch.3, p.43)
    let flash = device_peripherals.FLASH;
    let rcc = device_peripherals.RCC;
    let gpioa = device_peripherals.GPIOA;
    let usart1 = device_peripherals.USART1;

    // Figure 13 (page 93) shows how the clock of the microcontroller is
    // organised.

    
    hprintln!("Turning on HSE clock source");
    // - RCC_CR is a 32-bit register within the RCC peripheral (Section 6.3.1, 
    // page 102).

    rcc.cr.modify(|_, w| w.hseon().on());

    for i in 0..100 {
        hprintln!("Waiting for HSE Clock, round {}", i);
        if rcc.cr.read().hserdy().is_ready() {
            hprintln!("HSE Clock Ready");
            break;
        }
        if i > 95 {
            hprintln!("HSE Clock initiailisation failed")
        }
    }

    hprintln!("Setting up flash memory for >50MHz clock speed");

    flash.acr.modify(|_, w| w
        .dcen().enabled()
        .icen().enabled()
        .prften().enabled()
    );
    //We have to add wait states to accessing the flash memory if we want to run >30MHz
    //Refer to page 45, Section 3.4.1, Table 5
    flash.acr.modify(|_, w| w.latency().ws3());

    hprintln!("Setting up system clock dividers and switches");   

    rcc.cfgr.modify(|_, w| w
        .hpre().div1()
        .sw().pll()
    );
    // Clock options
    // Section 6.3.3, page 106

    hprintln!("Setting up PLL");

    rcc.pllcfgr.modify(|_, w| w.pllsrc().hse());    
    // Section 6.3.2, page 104

    unsafe {
        rcc.pllcfgr.modify(|_, w| w.pllm().bits(0b001100)); // divide by 12
        rcc.pllcfgr.modify(|_, w| w.plln().bits(0b001100000)); // multiply by 96
        rcc.pllcfgr.modify(|_, w| w.pllp().bits(0b00)); // divide by 2
    }

    rcc.cr.modify(|_, w| w.pllon().set_bit());
    //we turn on the PLL AFTER we set it up

    for i in 0..100 {
        hprintln!("Waiting for PLL, round {}", i);
        if rcc.cr.read().pllrdy().is_ready() {
            hprintln!("PLL READY");
            break;
        }
        if i > 95 {
            hprintln!("PLL initialisation failed")
        }
    }

    hprintln!("Setting up UART"); 
    

    // Enable the clock on GPIOB
    //      Section 6.3.9, page 117
    rcc.ahb1enr.modify(|_, w| w.gpioaen().enabled());
    // Enable the clock for USART 1
    //      Section 6.3.12, page 121
    rcc.apb2enr.modify(|_, w| w.usart1en().enabled());
    // Set pins 6 and 7 on GPIOB into 'alternate function' mode
    //      Section 8.4.1, page 157 
    gpioa.moder.modify(|_, w| w.moder9().alternate());
    gpioa.moder.modify(|_, w| w.moder10().alternate());
    // Select the 'alternate function' for pins 6 and 7
    //      Section 8.4.9, page 161
    //      Set this to 'AF07' as per datasheet Table 9, page 47 onwards
    gpioa.afrh.modify(|_, w| w.afrh9().af7());
    gpioa.afrh.modify(|_, w| w.afrh10().af7());
    // Set the outport speed register for pins 6 and 7 to high speed
    //      Section 8.4.3, page 158
    //      We could try higher speed....
    gpioa.ospeedr.modify(|_,w| w.ospeedr9().high_speed());
    gpioa.ospeedr.modify(|_,w| w.ospeedr10().high_speed());
    // Set the output type to be push pull
    //      Section 8.4.2, page 157
    gpioa.otyper.modify(|_,w| w.ot9().push_pull());
    gpioa.otyper.modify(|_,w| w.ot10().push_pull());
    // Set pull up resistor
    //      Section 8.4.4, page 158
    gpioa.pupdr.modify(|_,w| w.pupdr9().pull_up());
    gpioa.pupdr.modify(|_,w| w.pupdr10().pull_up());

    // Set USART word length
    //      Section 19.6.4, page 550
    usart1.cr1.modify(|_,w| w.m().m8());
    // Set oversampling mode
    //      Section 19.6.4, page 550
    usart1.cr1.modify(|_,w| w.over8().oversample16());        
    // Set baud rate
    //      You will need to read section 19.3.4, page 518
    //      It was not easy to understand
    //      I found these sites a much better explanation of the maths:
    //      https://electronics.stackexchange.com/questions/502135/understanding-calculations-for-baud-rate-fractional-generator-stm32f4
    // These two lines set up 9600 baud communication - and they work at 96MHz - NOT 100MHz
    /*
    usart1.brr.modify(|_,w| w.div_mantissa().bits(0b1001110001));  //625 - would need to be set to 651 for 100MHz operations
    usart1.brr.modify(|_,w| w.div_fraction().bits(0b0000)); //0 - would need to be set to 1 for 100MHz operations
     */
    // These two lines set up a baud rate of 115200
    /*
    usart1.brr.modify(|_,w| w.div_mantissa().bits(0b00000110110));  // 54
    usart1.brr.modify(|_,w| w.div_fraction().bits(0b00100)); // 4    
    */

    // These two lines set up a baud rate of 2,000,000
    // It works on linux with the CH431 Serial to USB chip    
    usart1.brr.modify(|_,w| w.div_mantissa().bits(0b00000000011));  // 3
    usart1.brr.modify(|_,w| w.div_fraction().bits(0b00010)); // 2



    // Set numbers of stop bits
    usart1.cr2.modify(|_,w| w.stop().stop1());
    // Enable USART1
    usart1.cr1.modify(|_,w| w.ue().enabled());
    // Enable Tx
    usart1.cr1.modify(|_,w| w.te().enabled());
    // Send Test byte
    while !usart1.sr.read().txe().bit_is_set() {}
    unsafe{
        usart1.dr.modify(|_,w| w.bits(u32::from(b'X')));
    }

    hprintln!("Finished setting up UART"); 

    hprintln!("Microcontroller continuing");

    loop {

//        delay(10_000_000);
        for i in 33..126 {
            while !usart1.sr.read().txe().bit_is_set() {}
            let value :u32 = i;
            unsafe{
                usart1.dr.modify(|_,w| w.bits(u32::from(value)));
            }
            while !usart1.sr.read().txe().bit_is_set() {}
            unsafe{
                usart1.dr.modify(|_,w| w.bits(0x0A)); //0x0A is hex for line feed - newline
            }
            while !usart1.sr.read().txe().bit_is_set() {}
            unsafe{
                usart1.dr.modify(|_,w| w.bits(13)); //0x0D is hex for carriage return - start of line
            }
            delay(9_600_000);
        }


//        hprintln!("Hello again");
        
         
        

    }

}
