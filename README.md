# STM32F411CE (BlackPill) Rust Code Examples

This repository contains pieces of code that I wrote and commented for the STM32F411CE (BlackPill) from WeAct.

The early examples are where I tried to learn about interacting with the microcontroller at a relatively low level by using the [Rust STM32F4 peripheral access crate](https://crates.io/crates/stm32f4) to access registers.
The examples include:
- Getting the system clock to run at 100MHz
- Sending serial messages at 2Mbps
- Timer-based interrupts (in progress)
- Arrays (future)
- Obtaining ADC values (future)
- Using direct memory access (future)

Once I have learned about accessing the registers directly, I will use Rust's HAL modules (i.e. [Rust HAL module for STM32F4xx](https://crates.io/crates/stm32f4xx-hal) or [embedded-hal](https://crates.io/crates/embedded-hal)) to do some more complicated stuff.

I put this together as a reference (mainly for myself) because I wanted to learn how to use Rust on the STM32 line of microcontrollers, but found it hard to work out how to set up the microcontroller in Rust. 
For example - it took me two full days of coding and debugging to get the clock running at 100MHz.

While I could just use the CubeMX software to generate the required setup code, this code is only available in C and the code that is generated still uses some form of hardware abstraction layer which means that you can't easily see the link between the code and the microcontroller datasheet.

The comments in this repository are not meant to provide an overview of programming in Rust or an introduction to using Rust in embedded systems.
These two things are well documented by the Rust team. 

I hope that others getting started with STM32 BlackPill (or maybe STM32 more generally) and wanting to learn how to program them at a relatively low level in Rust will find these examples helpful. 
I hope that the comments will prevent me from experiencing this (with thanks to @JenMsft for this picture):

![What is this code?](Gandalf.jpg "How I don't want to feel in a year's time")

